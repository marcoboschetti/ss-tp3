/**
 * Created by tritoon on 05/09/16.
 */
public class Wall {

    private double x, y, dimension;
    private boolean isVertical;


    public Wall(double x, double y, double dimension, boolean isVertical) {
        this.x = x;
        this.y = y;
        this.dimension = dimension;
        this.isVertical = isVertical;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getDimension() {
        return dimension;
    }

    public boolean isVertical() {
        return isVertical;
    }
}
