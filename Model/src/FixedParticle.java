/**
 * Created by juan on 08/09/16.
 */
public class FixedParticle extends Particle {
    public FixedParticle(double x, double y, double radius) {
        super(x, y, 0, 0, 0, Double.MAX_VALUE);
    }

    @Override
    public void setVelX(double velX) {
    }

    @Override
    public void setVelY(double velY) {
    }
}
