/**
 *r Created by tritoon on 10/08/16.
 */
public class Particle {
    private double x, y, velX, velY, radius, angle;
    private int id;
    private static int idCounter = 0;
    private double mass;
    private Color color;

    public static void resetId(){
        idCounter = 0;
    }

    public Particle(double x, double y, double radius) {
        this.x = x;
        this.y = y;
        this.color = new Color();
        this.radius = radius;
        this.mass = 0;
        id = idCounter++;
    }

    public Particle(double x, double y, double velx, double vely, double radius, double mass) {
        this.x = x;
        this.y = y;
        this.color = new Color();
        this.radius = radius;
        id = idCounter++;
        this.velX = velx;
        this.velY = vely;
        this.angle = Math.atan2(velx, vely);
        this.mass = mass;
    }

    public void setColor(Color newColor){
        this.color.setR(newColor.getR());
        this.color.setG(newColor.getG());
        this.color.setB(newColor.getB());
    }

    public void setColor(double R, double G, double B){
        this.color.setR(R);
        this.color.setG(G);
        this.color.setB(B);
    }

    public double getVelAngle(){
        return this.angle;
    }

    public void updateAngle(double angle) {
        this.angle = angle;
        this.velX = Math.sin(angle);
        this.velY = Math.cos(angle);
    }

    public static void resetIds() {
        idCounter = 0;
    }

    public Color getColor() {
        return color;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getVelX() {
        return velX;
    }

    public void setVelX(double velX) {
        this.velX = velX;
    }

    public double getVelY() {
        return velY;
    }

    public void setVelY(double velY) {
        this.velY = velY;
    }

    public double getMass() {
        return mass;
    }

    public int getId() {
        return id;
    }

    public void setMass(double mass) {
        this.mass = mass;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Particle particle = (Particle) o;

        return Double.compare(particle.id, id) == 0;
    }

    @Override
    public int hashCode() {
        long temp = Double.doubleToLongBits(id);
        return (int) (temp ^ (temp >>> 32));
    }

}
