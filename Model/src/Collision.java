/**
 * Created by juan on 05/09/16.
 */
public abstract class Collision {
    private double tc;

    public Collision(double tc){
        this.tc = tc;
    }

    public abstract void collide();

    public double getTc(){
        return this.tc;
    }

    public abstract Particle[] getParticles();
}
