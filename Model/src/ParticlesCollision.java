/**
 * Created by juan on 05/09/16.
 */
public class ParticlesCollision extends Collision {

    private Particle p1, p2;

    public void collide() {

        double deltaX = p1.getX() - p2.getX();
        double deltaY = p1.getY() - p2.getY();
        double sigma = p1.getRadius() + p2.getRadius();

        double deltaRX = p1.getX() - p2.getX();
        double deltaRY = p1.getY() - p2.getY();
        double deltaVX = p1.getVelX() - p2.getVelX();
        double deltaVY = p1.getVelY() - p2.getVelY();

        double prodVR = deltaRX * deltaVX + deltaRY * deltaVY;

        double j1 = 2 * p1.getMass() * prodVR /
                (sigma * (p1.getMass() / p2.getMass() + 1));
        double j2 = 2 * p2.getMass() * prodVR /
                (sigma * (1 + p2.getMass() / p1.getMass()));
        double jx1 = j1 * deltaX / sigma;
        double jy1 = j1 * deltaY / sigma;
        double jx2 = j2 * deltaX / sigma;
        double jy2 = j2 * deltaY / sigma;


        double moduleBefore = Math.sqrt(Math.pow(p1.getVelX() - p2.getVelX(), 2) + Math.pow(p1.getVelY() - p2.getVelY(), 2));

/*
        if (p1 instanceof FixedParticle || p2 instanceof FixedParticle) {
            System.out.println(String.format("prevX: %f prevY: %f", p1.getVelX(), p1.getVelY()));
            System.out.println(String.format("prevX: %f prevY: %f", p2.getVelX(), p2.getVelY()));
        }
*/
        p1.setVelX(p1.getVelX() - jx1 / p1.getMass());
        p1.setVelY(p1.getVelY() - jy1 / p1.getMass());
        p2.setVelX(p2.getVelX() + jx2 / p2.getMass());
        p2.setVelY(p2.getVelY() + jy2 / p2.getMass());
/*
        if (p1 instanceof FixedParticle || p2 instanceof FixedParticle) {
            System.out.println(String.format("prevX: %f prevY: %f", p1.getVelX(), p1.getVelY()));
            System.out.println(String.format("prevX: %f prevY: %f", p2.getVelX(), p2.getVelY()));
        }
*/
        double moduleAfter = Math.sqrt(Math.pow(p1.getVelX() - p2.getVelX(), 2) + Math.pow(p1.getVelY() - p2.getVelY(), 2));


        if (moduleAfter - moduleBefore >= 0.000001) {
      //      System.out.println("NOO" + moduleBefore + "," + moduleBefore);
         //   throw new IllegalStateException();
        }

    }

    @Override
    public Particle[] getParticles() {
        Particle[] particles = {p1, p2};
        return particles;
    }

    public String printVel() {
        return String.format("V1(%f,%f),V2(%f,%f)", p1.getVelX(), p1.getVelY(), p2.getVelX(), p2.getVelY());
    }

    public ParticlesCollision(Particle p1, Particle p2, double tc) {
        super(tc);
        this.p1 = p1;
        this.p2 = p2;
    }
}
