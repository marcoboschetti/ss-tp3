/**
 * Created by juan on 05/09/16.
 */
public class WallCollision extends Collision {

    private Particle p1;
    private Wall wall;

    public void collide() {
        if (wall.isVertical()) {
            p1.setVelX(p1.getVelX() * (-1));
        } else {
            p1.setVelY(p1.getVelY() * (-1));
        }
    }

    @Override
    public Particle[] getParticles() {
        Particle[] particles = {p1};
        return particles;
    }

    public WallCollision(Particle p1, Wall wall1, double tc) {
        super(tc);
        this.p1 = p1;
        this.wall = wall1;
    }

    public String printVel() {
        return String.format("V1(%f,%f)", p1.getVelX(), p1.getVelY());
    }

    public Particle getP1() {
        return p1;
    }

    public Wall getWall() {
        return wall;
    }
}
