import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;

/**
 * Created by tritoon on 10/08/16.
 */
public class Simulator {

    private static final int WALL_PARAMETERS = 4;
    private static final int WALLS_COUNT = 6;
    private static final boolean borderCondition = true;
    private static final double STEP = 0.001;
    private List<Particle> particles;
    private List<Wall> walls;
    private PriorityQueue<Collision> queue;
    private List<DataEntry> stats;

    public List<DataEntry> simulate(String staticData, String dinamicData, double W, double H, double fpDelta,
                                    String fileXYZ, double printDelta) {

        if (fpDelta > 1) {
            throw new IllegalArgumentException("Check that fpDelta value, thanks =)");
        }


        particles = new ArrayList<>();
        walls = new ArrayList<>(WALLS_COUNT);
        queue = new PriorityQueue<>((c1, c2) -> {
            if (c1.getTc() < c2.getTc())
                return -1;
            return 1;
        });

        stats = new LinkedList<>();

        double maxRadius = 0;

        try {
            BufferedReader staticInputBufferedReader = new BufferedReader(new FileReader(staticData));
            BufferedReader dynamicInputbufferedReader = new BufferedReader(new FileReader(dinamicData));

            staticInputBufferedReader.readLine();
            String line[];
            while ((line = staticInputBufferedReader.readLine().split(",")).length == WALL_PARAMETERS) {
                walls.add(new Wall(Double.parseDouble(line[0]), Double.parseDouble(line[1]),
                        Double.parseDouble(line[2]), line[3].equals("V")));
            }

            String[] radiuses = staticInputBufferedReader.readLine().split(",");

            staticInputBufferedReader.close();

            for (String radius : radiuses) {
                double doubleRadius = Double.parseDouble(radius);
                String[] position;
                position = dynamicInputbufferedReader.readLine().split(" ");
                double x = Double.parseDouble(position[0]);
                double y = Double.parseDouble(position[1]);
                double velX = Double.parseDouble(position[2]);
                double velY = Double.parseDouble(position[3]);
                double mass = Double.parseDouble(position[4]);
                particles.add(new Particle(x, y, velX, velY, doubleRadius, mass));
                if (doubleRadius > maxRadius) {
                    maxRadius = doubleRadius;
                }
            }

            for (Wall wall : walls) {
                particles.add(new FixedParticle(wall.getX(), wall.getY(), 0));
                if (wall.isVertical()) {
                    particles.add(new FixedParticle(wall.getX(), wall.getY() + wall.getDimension(), 0));
                } else {
                    particles.add(new FixedParticle(wall.getX() + wall.getDimension(), wall.getY(), 0));
                }
            }

          //  Color.paintPool(particles);

            dynamicInputbufferedReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        fileXYZ = String.format("%sN%dW%.3fH%.3fR%.4f.xyz", fileXYZ, particles.size(), W, H, particles.get(0).getRadius());

        List<Particle> corners = new ArrayList<>();

        wallsToParticles(corners, walls);
        double VIEWPORT_DELTA = 0.005;
        Particle viewport = new Particle((-1) * VIEWPORT_DELTA, (-1) * VIEWPORT_DELTA, 0);
        viewport.setMass(9);
        corners.add(viewport);
        viewport = new Particle((-1) * VIEWPORT_DELTA, H + VIEWPORT_DELTA, 0);
        viewport.setMass(9);
        corners.add(viewport);
        viewport = new Particle(W + VIEWPORT_DELTA, (-1) * VIEWPORT_DELTA, 0);
        viewport.setMass(9);
        corners.add(viewport);
        viewport = new Particle(W + VIEWPORT_DELTA, H + VIEWPORT_DELTA, 0);
        viewport.setMass(9);
        corners.add(viewport);


        PrinterXYZ.startPrinting(fileXYZ);

        double fp = calculateFp(particles, W);
        double timeCounter = 0, totalTime = 0, currentTc = 0;

        final double MAX_TIME = Double.MAX_VALUE;

        PrinterXYZ.printTimeStep(particles, corners, totalTime);

        queueCollisions();
        PressureManager.initialize(particles, corners, walls, W);
        Collision collision;
        stats.add(new DataEntry(0, 1));
        while (totalTime < MAX_TIME && (fp < 0.5 - fpDelta || fp > 0.5 + fpDelta)) {

            collision = queue.poll();

            timeCounter = currentTc;
            double nextTime = collision.getTc();

            while (timeCounter + printDelta < nextTime) {
                ColorManager.pressureColor(particles, PressureManager.getParticlePressureMeter());
//                        ColorManager.pressureColor(corners, PressureManager.getWallPressureMeter());
                moveParticles(printDelta, W, H);
                timeCounter += printDelta;
                PrinterXYZ.printTimeStep(particles, corners, timeCounter);
                System.out.println(fp + "," + totalTime + ", P:" + PressureManager.getSystemPressure());
            }

            moveParticles(nextTime - timeCounter, W, H);

            collision.collide();
            PressureManager.informCollision(collision, nextTime - timeCounter);

            currentTc = collision.getTc();
            for (Particle particle : collision.getParticles()) {
                updateQueue(particle, currentTc);
            }


            fp = calculateFp(particles, W);
            stats.add(new DataEntry(currentTc, fp));
        }

        //PrinterXYZ.printTimeStep(particles, corners, totalTime);

        PrinterXYZ.endPrinting();

        return stats;
    }

    private double calculateFp(List<Particle> particles, double W) {
        double leftCount = 0;
        for (Particle p : particles) {
            if (p.getX() < W / 2) {
                leftCount++;
            }
        }
        return leftCount / particles.size();
    }

    private void moveParticles(double tc, double W, double H) {
        for (Particle p : particles) {
            double nextX = p.getX() + p.getVelX() * tc;
            double nextY = p.getY() + p.getVelY() * tc;

            if (borderCondition) {
                if (nextX <= 0 || nextX >= W) {
                    p.setVelX(p.getVelX() * (-1));
                    nextX = p.getX() + p.getVelX() * tc;
                }
                if (nextY <= 0 || nextY >= H) {
                    p.setVelY(p.getVelY() * (-1));
                    nextY = p.getY() + p.getVelY() * tc;
                }
            }

            p.setX(nextX);
            p.setY(nextY);
        }
    }

    private Collision checkMinTc() {
        double minTc = -1;
        Collision currentCollition = null;
        int size = particles.size(),
                wallsSize = walls.size();
        for (int i = 0; i < size; i++) {
            Particle current = particles.get(i);
            for (int j = i + 1; j < size; j++) {
                double tc = getTc(particles.get(j), current);
                if (tc != -1 && (tc < minTc || minTc == -1)) {
                    minTc = tc;
                    currentCollition = new ParticlesCollision(current, particles.get(j), tc);
                }
            }
            for (int j = 0; j < wallsSize; j++) {
                double tc = getTc(current, walls.get(j));
                if (tc != -1 && (tc < minTc || minTc == -1)) {
                    minTc = tc;
                    currentCollition = new WallCollision(current, walls.get(j), tc);
                }
            }
        }

        return currentCollition;
    }

    private void queueCollisions() {
        int size = particles.size(),
                wallsSize = walls.size();
        for (int i = 0; i < size; i++) {
            Particle current = particles.get(i);
            for (int j = i + 1; j < size; j++) {
                double tc = getTc(particles.get(j), current);
                if (tc >= 0)
                    queue.offer(new ParticlesCollision(current, particles.get(j), tc));
            }
            for (int j = 0; j < wallsSize; j++) {
                double tc = getTc(current, walls.get(j));
                if (tc >= 0)
                    queue.offer(new WallCollision(current, walls.get(j), tc));
            }
        }
        //  System.out.println("queue size: " + queue.size());
    }

    private void updateQueue(Particle particle, double totalTime) {
        int size = particles.size(),
                wallsSize = walls.size();
        queue.removeIf(collision -> {
            for (Particle current : collision.getParticles()) {
                if (current.equals(particle))
                    return true;
            }
            return false;
        });
        for (int j = 0; j < size; j++) {
            double tc = getTc(particles.get(j), particle);
            if (tc > 0)
                queue.offer(new ParticlesCollision(particle, particles.get(j), tc + totalTime));
        }
        for (int j = 0; j < wallsSize; j++) {
            double tc = getTc(particle, walls.get(j));
            if (tc > 0)
                queue.offer(new WallCollision(particle, walls.get(j), tc + totalTime));
        }
        //   System.out.println("updated queue size: " + queue.size());

    }

    private double getTc(Particle p1, Particle p2) {

        double dX = p2.getX() - p1.getX(),
                dY = p2.getY() - p1.getY(),
                dVX = p2.getVelX() - p1.getVelX(),
                dVY = p2.getVelY() - p1.getVelY(),
                dSqrR = dX * dX + dY * dY,
                dSqrV = dVX * dVX + dVY * dVY,
                dRV = dX * dVX + dY * dVY,
                collisionD = p1.getRadius() + p2.getRadius(),
                d = dRV * dRV - dSqrV * (dSqrR - collisionD * collisionD);


        if (dRV >= 0 || d < 0)
            return -1;
        else {
            double tc = -(dRV + Math.sqrt(d)) / dSqrV;
            if (tc < 0.0)
                return -1;
            return tc;
        }
    }

    private double getTc(Particle p1, Wall wall1) {
        double timeLeft;
        if (wall1.isVertical()) {
            if (p1.getVelX() < 0.0 && p1.getX() >= wall1.getX()) {
                timeLeft = Math.max((wall1.getX() - (p1.getX() - p1.getRadius())) / p1.getVelX(), 0.0);
                if (timeLeft < 0.0)
                    System.out.println("falla");
                double newY = p1.getVelY() * timeLeft + p1.getY();
                if (newY >= wall1.getY() && newY <= wall1.getY() + wall1.getDimension()) {
                    return timeLeft;
                }
            } else if (p1.getVelX() > 0.0 && p1.getX() <= wall1.getX()) {
                timeLeft = Math.max((wall1.getX() - (p1.getX() + p1.getRadius())) / p1.getVelX(), 0.0);
                if (timeLeft < 0.0)
                    System.out.println("falla");
                double newY = p1.getVelY() * timeLeft + p1.getY();
                if (newY >= wall1.getY() && newY <= wall1.getY() + wall1.getDimension()) {
                    return timeLeft;
                }
            }
        } else {
            if (p1.getVelY() < 0.0 && p1.getY() >= wall1.getY()) {
                timeLeft = Math.max((wall1.getY() - (p1.getY() - p1.getRadius())) / p1.getVelY(), 0.0);
                if (timeLeft < 0.0)
                    System.out.println("falla");
                double newX = p1.getVelX() * timeLeft + p1.getX();
                if (newX >= wall1.getX() && newX <= wall1.getX() + wall1.getDimension()) {
                    return timeLeft;
                }
            } else if (p1.getVelY() > 0.0 && p1.getY() <= wall1.getY()) {
                timeLeft = Math.max((wall1.getY() - (p1.getY() + p1.getRadius())) / p1.getVelY(), 0.0);
                if (timeLeft < 0.0)
                    System.out.println("falla");
                double newX = p1.getVelX() * timeLeft + p1.getX();
                if (newX >= wall1.getX() && newX <= wall1.getX() + wall1.getDimension()) {
                    return timeLeft;
                }
            }
        }
        return -1;
    }



    private void wallsToParticles(List<Particle> corners, List<Wall> walls) {
        int type = 1;
        for (Wall w : walls) {
            //   System.out.println("X:" + w.getX() + ",Y:" + w.getY() + ",dim:" + w.getDimension() + (w.isVertical() ? "V" : "H"));
            if (w.isVertical()) {
                for (double y = w.getY(); y < w.getY() + w.getDimension(); y += STEP) {
                    Particle p = new Particle(w.getX(), y, 0);
                    p.setColor(3, 0, 0);
                    p.setMass(type);
                    corners.add(p);
                }
                Particle p = new Particle(w.getX(), w.getY() + w.getDimension(), 0);
                p.setColor(3, 0, 0);
                p.setMass(type);
                corners.add(p);
            } else {
                for (double x = w.getX(); x < w.getX() + w.getDimension(); x += STEP) {
                    Particle p = new Particle(x, w.getY(), 0);
                    p.setColor(3, 0, 0);
                    p.setMass(type);
                    corners.add(p);
                }
                Particle p = new Particle(w.getX() + w.getDimension(), w.getY(), 0);
                p.setColor(3, 0, 0);
                p.setMass(type);
                corners.add(p);
            }
            type++;
        }
    }

}
