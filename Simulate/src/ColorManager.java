import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

/**
 * Created by tritoon on 22/08/16.
 */
public class ColorManager {

    public static void setHeatColor(Map<Particle, Set<Particle>> neighborMap, List<Particle> particles) {
        int maxAffected = 0;
        for (Set l : neighborMap.values()) {
            if (l.size() > maxAffected) {
                maxAffected = l.size();
            }
        }
        for (Particle p : particles) {
            Color.setHeatColor(p, maxAffected, neighborMap.get(p).size());
        }
    }

    public static void setAngleColor(List<Particle> particles) {
        for (Particle p : particles) {
            p.setColor(0.5, (Math.cos(p.getVelAngle()) + 1) / 2, (Math.sin(p.getVelAngle()) + 1) / 2);
        }
    }

    private static void highlight(Optional<Particle> particle, Map<Particle, Set<Particle>> neighborMap) {
        if (particle.isPresent()) {
            particle.get().setColor(new Color(0, 0, 1));
            Set<Particle> neighbors = neighborMap.get(particle.get());
            neighbors.forEach(x -> x.setColor(new Color(238.0 / 255, 130.0 / 255, 238.0 / 255)));
        } else {
            System.out.println("Particle not found");
        }
    }

    public static void pressureColor(List<Particle> particles, Map<Particle, Integer> pressures) {
        int maxAffected = 0;
        for (int i : pressures.values()) {
            if (i > maxAffected) {
                maxAffected = i;
            }
        }

        for (Particle p : particles) {
            Color.setHeatColor(p, maxAffected, pressures.get(p));
        }
    }
}
