import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by tritoon on 07/09/16.
 */
public class PressureManager {

    private static final double INFLUENCE = 0.0075;
    private static final int COLOR_PERSISTENCE = 10;
    private static final int COLOR_CAP = 250;


    private static double currentCollisionX, currentCollisionY;
    private static Map<Particle, Integer> wallPressureMeter;
    private static Map<Particle, Integer> particlePressureMeter;
    private static double currentAcumL, lastEndedAcumL, currentAcumR, lastEndedAcumR, deltaT, deltaL, currentTimeAcum;
    private static double W;

    public static void initialize(List<Particle> particles, List<Particle> corners, List<Wall> walls, double W_MAP) {
        W = W_MAP;
        deltaT = 1;
        deltaL = 0;
        for (Wall w : walls) {
            deltaL += w.getDimension();
        }
        lastEndedAcumL = 0;
        lastEndedAcumR = 0;
        currentTimeAcum = 0;

        wallPressureMeter = new HashMap<>();
        particlePressureMeter = new HashMap<>();

        for (Particle p : particles) {
            particlePressureMeter.put(p, 1);
        }
        for (Particle p : corners) {
            wallPressureMeter.put(p, 1);
        }
    }


    public static void informCollision(Collision c, double relativeTc) {
        if (c instanceof ParticlesCollision) {
            handleParticleColission((ParticlesCollision) c);
        } else {
            handleWallColission((WallCollision) c, relativeTc);
        }
    }

    private static void handleParticleColission(ParticlesCollision c) {
        for (Particle p : particlePressureMeter.keySet()) {
            if (p.equals(c.getParticles()[0]) || p.equals(c.getParticles()[1])) {
                particlePressureMeter.put(p, Math.max(COLOR_CAP, particlePressureMeter.get(p) + COLOR_PERSISTENCE));
            } else {
                particlePressureMeter.put(p, Math.max(0, particlePressureMeter.get(p) - 3));
            }
        }
    }

    private static void handleWallColission(WallCollision wc, double relativeTc) {
        calculateCollisionPoint(wc);
        for (Particle p : wallPressureMeter.keySet()) {
            double distance = Math.pow(p.getX() - currentCollisionX, 2) + Math.pow(p.getY() - currentCollisionY, 2);
            if (distance <= INFLUENCE * INFLUENCE) {
                wallPressureMeter.put(p, Math.max(COLOR_CAP, (int) (wallPressureMeter.get(p) + (COLOR_PERSISTENCE * distance / INFLUENCE))));
            } else {
                wallPressureMeter.put(p, Math.max(0, wallPressureMeter.get(p) - 3));
            }
        }

        for (Particle p : particlePressureMeter.keySet()) {
            if (p.equals(wc.getP1())) {
                particlePressureMeter.put(p, Math.max(COLOR_CAP, particlePressureMeter.get(p) + COLOR_PERSISTENCE));
            } else {
                particlePressureMeter.put(p, Math.max(0, particlePressureMeter.get(p) - 3));
            }
        }

        //Calculates the real pressure variable
        Particle p = wc.getP1();
        currentTimeAcum += relativeTc;
        //   System.out.println("currentTImeAcum "+currentTimeAcum);
        if (p.getX() < W / 2) {
            currentAcumL += p.getMass() * Math.pow(Math.pow(p.getVelX(), 2) + Math.pow(p.getVelY(), 2), 0.5);
        } else {
            currentAcumR += p.getMass() * Math.pow(Math.pow(p.getVelX(), 2) + Math.pow(p.getVelY(), 2), 0.5);
        }
        if (currentTimeAcum >= deltaT) {
            lastEndedAcumL = currentAcumL;
            currentAcumL = 0;
            lastEndedAcumR = currentAcumR;
            currentAcumR = 0;
            currentTimeAcum = 0;
        }
    }


    public static double getSystemPressure() {
        return (lastEndedAcumL + lastEndedAcumR) * 2 / (deltaL * deltaT);
    }

    public static double getLeftSystemPressure() {
        return (lastEndedAcumL) * 2 / (deltaL * deltaT);
    }

    public static double getRightSystemPressure() {
        return (lastEndedAcumR) * 2 / (deltaL * deltaT);
    }

    private static void calculateCollisionPoint(WallCollision wc) {
        Wall wall1 = wc.getWall();
        Particle p1 = wc.getP1();
        double timeLeft;

        if (wall1.isVertical()) {
            if (p1.getVelX() < 0.0) {
                timeLeft = (wall1.getX() - (p1.getX() - p1.getRadius())) / p1.getVelX();
                double newY = p1.getVelY() * timeLeft + p1.getY();
                currentCollisionX = wall1.getX();
                currentCollisionY = newY;

            } else {
                timeLeft = (wall1.getX() - (p1.getX() + p1.getRadius())) / p1.getVelX();
                double newY = p1.getVelY() * timeLeft + p1.getY();
                currentCollisionX = wall1.getX();
                currentCollisionY = newY;
            }
        } else {
            if (p1.getVelY() < 0.0) {
                timeLeft = (wall1.getY() - (p1.getY() - p1.getRadius())) / p1.getVelY();
                double newX = p1.getVelX() * timeLeft + p1.getX();
                currentCollisionX = newX;
                currentCollisionY = wall1.getY();
            } else {
                timeLeft = (wall1.getY() - (p1.getY() + p1.getRadius())) / p1.getVelY();
                double newX = p1.getVelX() * timeLeft + p1.getX();
                currentCollisionX = newX;
                currentCollisionY = wall1.getY();
            }
        }
    }

    public static Map<Particle, Integer> getWallPressureMeter() {
        return wallPressureMeter;
    }

    public static Map<Particle, Integer> getParticlePressureMeter() {
        return particlePressureMeter;
    }
}
