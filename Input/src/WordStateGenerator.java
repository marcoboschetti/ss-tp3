import java.io.*;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by tritoon on 11/08/16.
 */
public class WordStateGenerator {


    /**
     * Writes 2 files with an initial states for the particles as indicated in Simulator.
     * The position is greater or equals to 0 and less than L for each particle.
     * Superposition is not checked by the generator
     * <p>
     * 0,maxY+2S                        maxX+2S,maxY+2S
     * S
     * FUNDAMENTO
     * S
     * 0,0                              maxX+2S,0
     *
     * @param minRadius
     * @param maxRadius
     * @param staticFilename
     * @param dinamicFilename
     */

    static final int SPAN = 10;
    private static final int PRINT_DISOLUTION = 1;

    public double generateRandomState(String inputArtTest, double radius, double mass, double velMod, String staticFilename, String dinamicFilename) {

        radius = radius * PRINT_DISOLUTION;
        BufferedReader inputReader = null;
        PrintWriter writer = null;
        double L = 0;
        int particles = 0;
        Queue<String> fullFile = new LinkedList<>();
        int maxX = 0;
        int maxY = 0;
        int SIDE = (int) (radius * 3);
        try {
            inputReader = new BufferedReader(new FileReader(inputArtTest));
            String line;
            while ((line = inputReader.readLine()) != null) {
                fullFile.offer(line);
                maxX = Math.max(maxX, line.length());
            }
            maxY = fullFile.size();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("MAXx" + maxX + ", MAXy" + maxY);

        int y = maxY;
        try {
            writer = new PrintWriter(dinamicFilename, "UTF-8");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

       // writer.write(String.format("%d %f %f %f %f\n", (maxX / 2) * SIDE + SPAN, maxY * SIDE + (SPAN * 0.45), velMod, velMod * (-1), mass));

        while (!fullFile.isEmpty()) {
            String line = fullFile.poll();
            for (int x = 0; x < line.length(); x++) {
                char c = line.charAt(x);

                if (c != ' ' && x % PRINT_DISOLUTION == 0 && y % PRINT_DISOLUTION == 0) {
                     double ang = Math.random() * 2 * Math.PI;

                    writer.write(String.format("%d %d %f %f %f\n", x * SIDE + SPAN, y * SIDE + SPAN,  Math.sin(ang) * velMod, Math.cos(ang) * velMod,
                            mass));
                    particles++;
                    if (L < x || L < y) {
                        L = Math.max(x, y);
                    }
                }
            }
            y--;
        }
        writer.close();


        try {
            writer = new PrintWriter(staticFilename, "UTF-8");
        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        double W = maxX * SIDE + SPAN * 2, H = maxY * SIDE + SPAN * 2;
        LinkedList<Wall> walls = new LinkedList<>();
        writer.write("Walls: x, y, dim, isVertical\n");
        walls.add(new Wall(0, 0, W, false));
        walls.add(new Wall(0, 0, H, true));
        walls.add(new Wall(W, 0, H, true));
        walls.add(new Wall(0, H, W, false));
        System.out.println(W + "," + H);
        for (Wall w : walls) {
            writer.write(String.format("%f,%f,%f,%c\n", w.getX(), w.getY(), w.getDimension(), (w.isVertical() ? 'V' : 'H')));
        }

        writer.write("Particles\n");
      //  writer.write(String.format("%f,", radius*10));

        for (int i = 0; i < particles; i++) {

            writer.write(String.format("%f", radius));
            if (i < particles - 1) {
                writer.write(",");
            }
        }

        writer.close();
        return (Math.max(maxX, maxY) + 1) * SIDE;

    }

}
