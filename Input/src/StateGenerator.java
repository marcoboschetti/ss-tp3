import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by tritoon on 11/08/16.
 */
public class StateGenerator {


    /**
     * Writes 2 files with an initial states for the particles as indicated in Simulator.
     * The position is greater or equals to 0 and less than L for each particle.
     * Superposition is not checked by the generator
     *
     * @param L
     * @param particles
     * @param minRadius
     * @param maxRadius
     * @param staticFilename
     * @param dinamicFilename
     */

    private static final int MAX_TRIES_PER_PARTICLE = 100;
    private static final int BROWNIES = 0;


    public int generateRandomState(double W, double H, int N, double radius, double openingL,
                                   double vel, double mass, String staticFilename, String dinamicFilename) {
        return generateState(W, H, N, radius, radius, openingL, vel, vel, mass, mass, staticFilename, dinamicFilename);
    }

    public void generateRandomState(double W, double H, int N, double minRadius, double maxRadius, double openingL,
                                    double minVel, double maxVel, double minMass, double maxMass,
                                    String staticFilename, String dinamicFilename) {
        generateState(W, H, N, minRadius, maxRadius, openingL, minVel, maxVel, minMass, maxMass, staticFilename, dinamicFilename);
    }

    private int generateState(double W, double H, int N, double minRadius, double maxRadius, double openingL,
                              double minVel, double maxVel, double minMass, double maxMass, String staticFilename, String dinamicFilename) {


        List<Particle> particles = new ArrayList<>(N);
        List<Wall> walls = new ArrayList<>(6);

        PrintWriter writer = null;
        try {
            writer = new PrintWriter(staticFilename, "UTF-8");
        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        writer.write("Walls: x, y, dim, isVertical\n");
        walls.add(new Wall(0, 0, W, false));
        walls.add(new Wall(0, 0, H, true));
        walls.add(new Wall(W, 0, H, true));
        walls.add(new Wall(0, H, W, false));
        //Making the separation
        walls.add(new Wall(W / 2, 0, (H - openingL) / 2, true));
        walls.add(new Wall(W / 2, (H - openingL) / 2 + openingL, (H - openingL) / 2, true));
        for (Wall w : walls) {
            writer.write(String.format("%f,%f,%f,%c\n", w.getX(), w.getY(), w.getDimension(), (w.isVertical() ? 'V' : 'H')));
        }

        writer.write("Particles\n");
        for (int i = 0; i < N; i++) {
            double radius = minRadius + Math.random() * (maxRadius - minRadius);
            if (i < BROWNIES)
                radius = radius * 10;
            writer.write(String.format("%f", radius));
            particles.add(i, new Particle(-1, -1, radius));
            if (i < N - 1) {
                writer.write(",");
            }
        }
        writer.close();

        try {
            writer = new PrintWriter(dinamicFilename, "UTF-8");
        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < N; i++) {
            double ang = Math.random() * 2 * Math.PI;
            double velMod = minVel + Math.random() * (maxVel - minVel);
            boolean put = false;
            for (int tries = 0; !put && tries < MAX_TRIES_PER_PARTICLE; tries++) {
                double x = (Math.random() * ((W / 2) - 2 * COLISION_DELTA)) + COLISION_DELTA;
                double y = (Math.random() * (H - 2 * COLISION_DELTA)) + COLISION_DELTA;

                if (!hasColision(i, x, y, particles, walls)) {
                    put = true;
                    particles.get(i).setX(x);
                    particles.get(i).setY(y);
                    double mass = minMass + Math.random() * (maxMass - minMass);
                    if (i < BROWNIES)
                        mass = mass * 10;
                    writer.write(String.format("%f %f %f %f %f\n", x, y, Math.sin(ang) * velMod, Math.cos(ang) * velMod,
                            mass));
                }
            }
            if (!put) {
                //      System.out.println((String.format("I can't create that, Im solid =/ I did put %d particles...", i)));
                return i;
            }
        }
        writer.close();
        return -1;
    }

    private final double COLISION_DELTA = 0.003;

    private boolean hasColision(int currentIndex, double x, double y, List<Particle> particles, List<Wall> walls) {
        Particle curP = particles.get(currentIndex);
        for (Wall w : walls) {
            if (w.isVertical()) {
                if (curP.getY() - curP.getRadius() >= w.getY() && curP.getY() + curP.getRadius() <= w.getY() + w.getDimension()
                        && curP.getX() - curP.getRadius() <= w.getX() - COLISION_DELTA && curP.getX() + curP.getRadius() >= w.getX() + COLISION_DELTA) {
                    return true;
                }
            } else {
                if (curP.getX() >= w.getX() && curP.getX() <= w.getX() + w.getDimension()
                        && curP.getY() - curP.getRadius() <= w.getY() - COLISION_DELTA && curP.getY() + curP.getRadius() >= w.getY() + COLISION_DELTA) {
                    return true;
                }
            }
        }


        for (int i = 0; i < currentIndex; i++) {
            Particle checking = particles.get(i);
            if (Math.pow(x - checking.getX(), 2) + Math.pow(y - checking.getY(), 2) <
                    Math.pow(curP.getRadius() + checking.getRadius() + COLISION_DELTA, 2)) {
                return true;
            }
        }
        return false;
    }

}
