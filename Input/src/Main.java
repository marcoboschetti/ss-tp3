import java.util.List;

/**
 * Created by tritoon on 10/08/16.
 */
public class Main {

    final static private String STATIC_FILE = "staticTest1",
            DYNAMIC_FILE = "dinamicTest1",
            FILE_XYZ = "status",
            M_FUNCTION_FILE = "timeFunction.csv";
    final static private int TRIES_PER_M = 5,
            PARTICLES = 106;
    final static private double W = 0.24,
            H = 0.09,
            FP_DELTA = 0.005,
            OPENING_L = 0.06,
            VELOCITY = 0.01,
            RADIUS = 0.0015,
            MASS = 1,
            PRINT_DELTA = 0.05;

    public static void main(String[] args) {

        singleTest();
    }

    private static void statsTest() {

        PrinterStatistics printer = new PrinterStatistics("opening-time-ratio.csv");

        for (double openingL = 0.09; openingL > 0.000; openingL -= 0.003) {
            new StateGenerator().generateRandomState(W, H, PARTICLES, RADIUS, openingL, VELOCITY, MASS, STATIC_FILE, DYNAMIC_FILE);
            Simulator simulator = new Simulator();
            List<DataEntry> stats = simulator.simulate(STATIC_FILE, DYNAMIC_FILE, W, H, FP_DELTA, FILE_XYZ, PRINT_DELTA);
            System.out.println(String.format("Opening: %f", openingL));
            printer.print(stats, String.format("Opening: %f", openingL));
        }
        printer.close();
    }

    private static void singleTest() {
        new StateGenerator().generateRandomState(W, H, PARTICLES, RADIUS, OPENING_L, VELOCITY, MASS, STATIC_FILE, DYNAMIC_FILE);
        new Simulator().simulate(STATIC_FILE, DYNAMIC_FILE, W, H, FP_DELTA, FILE_XYZ, PRINT_DELTA);
    }


}