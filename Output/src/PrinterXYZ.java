import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 * Created by tritoon on 22/08/16.
 */
public class PrinterXYZ {

    private static PrintWriter writer = null;

    public static void startPrinting(String fileName) {
        try {
            writer = new PrintWriter(fileName, "UTF-8");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    /**
     * Prints a simulation state, printing for each particle:
     *  ID RADIUS X Y VELOCITYX VELOCITYY R G B vecR vecG vecB
     * @param particles
     * @param corners
     * @param timeStep
     */
    public static void printTimeStep(List<Particle> particles, List<Particle> corners, double timeStep) {

        writer.println((particles.size() + corners.size()));
        writer.println(String.format("Time:%f", timeStep));

        for (Particle p : particles) {
            writer.println(String.format("%d %.5f %.5f %.5f %.4f %.4f %.3f %.3f %.3f %.3f %.3f %.3f 0", p.getId(), p.getRadius(),
                    p.getX(), p.getY(),p.getVelX(), p.getVelY(), p.getColor().getR(), p.getColor().getG(), p.getColor().getB(),
                    p.getColor().getR(), p.getColor().getG(), p.getColor().getB()));
        }

        for (Particle p : corners) {
            writer.println(String.format("%d %.5f %.5f %.5f %.4f %.4f %.3f %.3f %.3f %.3f %.3f %.3f %.0f", p.getId(), p.getRadius(),
                    p.getX(), p.getY(),p.getVelX(), p.getVelY(), p.getColor().getR(), p.getColor().getG(), p.getColor().getB(),
                    p.getColor().getR(), p.getColor().getG(), p.getColor().getB(), p.getMass()));
        }

    }

    public static void endPrinting() {
        writer.close();
        writer = null;
    }

}
