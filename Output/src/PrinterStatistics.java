import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 * Created by juan on 09/09/16.
 */
public class PrinterStatistics {
    private PrintWriter writer;

    public void print(List<DataEntry> data, String title) {
        writer.println(title);
        print(data);
    }

    public void print(List<DataEntry> data) {
        writer.println("time, fp");
        for (DataEntry entry : data) {
            writer.println(String.format("%f, %f", entry.getTime(), entry.getFp()));
        }
    }

    public PrinterStatistics(String fileName) {
        try {
            writer = new PrintWriter(fileName, "UTF-8");
        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public void close() {
        writer.close();
    }
}
